package com.team4.airhockey.engine;

/**
 * Created by ryu24 on 2017-04-25.
 */

public class InputController {

    public double mLeftHorizontalPosition;
    public double mLeftVerticalPosition;
    public boolean mLeftIsTouching;

    public double mRightHorizontalPosition;
    public double mRightVerticalPosition;
    public boolean mRightIsTouching;



    public void onStart(){}

    public void onStop(){}

    public void  onPause(){
    }

    public void onResume(){}

    public void onPreUpdate(){}

}
