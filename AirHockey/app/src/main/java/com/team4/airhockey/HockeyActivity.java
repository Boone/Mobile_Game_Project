package com.team4.airhockey;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;

import com.team4.airhockey.Fragments.GameFragment;
import com.team4.airhockey.Fragments.MainMenuFragment;

public class HockeyActivity extends Activity {

    private static final String TAG_FRAGMENT = "content";
    private GamepadControllerListener mGamepadControllerListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hockey);
        if (savedInstanceState == null){
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new MainMenuFragment(), TAG_FRAGMENT)
                    .commit();
        }

    }

    public  void  startGame(){
        navigateToFragment(new GameFragment());
    }

    public void settings(){
        navigateToFragment(new com.team4.airhockey.Fragments.SettingsFragment());
    }

    public void customize(){
        navigateToFragment(new com.team4.airhockey.Fragments.CustomizeFragment());
    }

    public void mainMenu(){navigateToFragment(new MainMenuFragment());}


    private void navigateToFragment(HockeyBaseFragment dst) {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, dst, TAG_FRAGMENT)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed(){
        final HockeyBaseFragment fragment = (HockeyBaseFragment) getFragmentManager().findFragmentByTag(TAG_FRAGMENT);
        if(fragment == null || !fragment.onBackPressed()){
            super.onBackPressed();
        }
    }
    public void navigateBack(){
        super.onBackPressed();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus){
            View decorView = getWindow().getDecorView();
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT){
                decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LOW_PROFILE);
            }
            else {
                decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            }
        }
    }

    public void setGamepadControllerListener(GamepadControllerListener listener) {
        mGamepadControllerListener = listener;
    }

    @Override
    public boolean dispatchGenericMotionEvent(MotionEvent event) {
        if (mGamepadControllerListener != null) {
            if (mGamepadControllerListener.dispatchGenericMotionEvent(event)) {
                return true;
            }
        }
        return super.dispatchGenericMotionEvent(event);
    }

    @Override
    public boolean dispatchKeyEvent (KeyEvent event) {
        if (mGamepadControllerListener != null) {
            if (mGamepadControllerListener.dispatchKeyEvent(event)) {
                return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }





}
