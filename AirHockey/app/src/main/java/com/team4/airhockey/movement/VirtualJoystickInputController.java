package com.team4.airhockey.movement;

import android.view.MotionEvent;
import android.view.View;

import com.team4.airhockey.R;
import com.team4.airhockey.engine.InputController;

/**
 * Created by ryu24 on 2017-04-28.
 */

public class VirtualJoystickInputController extends InputController {

    private final double mMaxDistance;

    public double xAxis;
    public double yAxis;

    public VirtualJoystickInputController(View view, int player) {
        if (player == 1) {
            view.findViewById(R.id.vjoystick_player1).setOnTouchListener(new VJoystickTouchListener());
        }
        else if (player == 2){
            view.findViewById(R.id.vjoystick_player2).setOnTouchListener(new VJoystickTouchListener());
        }

        double pixelFactor = view.getHeight() / 400d;
        mMaxDistance = 50*pixelFactor;
    }

    private class VJoystickTouchListener implements View.OnTouchListener {

        private float mStartingPositionX;
        private float mStartingPositionY;

        public VJoystickTouchListener() {
        }


        @Override
        public boolean onTouch(View v, MotionEvent event) {

            int action = event.getActionMasked();
            if (action == MotionEvent.ACTION_DOWN) {
                mStartingPositionX = event.getX(0);
                mStartingPositionY = event.getY(0);
            } else if (action == MotionEvent.ACTION_UP) {
                xAxis = 0;
                yAxis = 0;
            } else if (action == MotionEvent.ACTION_MOVE) {
                // Get the proportion to the max
                xAxis = (event.getX(0) - mStartingPositionX) / mMaxDistance;
                if (xAxis > 1) {
                    xAxis = 1;
                } else if (xAxis < -1) {
                    xAxis = -1;
                }



                yAxis = (event.getY(0) - mStartingPositionY) / mMaxDistance;
                if (yAxis > 1) {
                    yAxis = 1;
                } else if (yAxis < -1) {
                    yAxis = -1;
                }
            }

            return true;
        }
    }

//    private class VFireButtonTouchListener implements View.OnTouchListener {
//        @Override
//        public boolean onTouch(View v, MotionEvent event) {
//            int action = event.getActionMasked();
//            if (action == MotionEvent.ACTION_DOWN) {
//                mIsFiring = true;
//            }
//            else if (action == MotionEvent.ACTION_UP) {
//                mIsFiring = false;
//            }
//            return true;
//        }
//    }
}
