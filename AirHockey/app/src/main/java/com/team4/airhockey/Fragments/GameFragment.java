package com.team4.airhockey.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.hardware.input.InputManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.team4.airhockey.HockeyActivity;
import com.team4.airhockey.HockeyBaseFragment;
import com.team4.airhockey.engine.GameEngine;
import com.team4.airhockey.R;
import com.team4.airhockey.movement.CompositeInputController;
import com.team4.airhockey.movement.Goal;
import com.team4.airhockey.movement.Player;
import com.team4.airhockey.movement.Puck;
import com.team4.airhockey.movement.ScoreBoard;


/**
 * Created by ryu24 on 2017-04-24.
 */
@SuppressLint("NewApi")
public class GameFragment extends  HockeyBaseFragment implements View.OnClickListener, InputManager.InputDeviceListener {
    private GameEngine mGameEngine;
    int mMap;


    public GameFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_game, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.btn_play_pause).setOnClickListener(this);
        final ViewTreeObserver obs = view.getViewTreeObserver();
        obs.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    obs.removeGlobalOnLayoutListener(this);
                }
                else {
                    obs.removeOnGlobalLayoutListener(this);
                }

                mGameEngine = new GameEngine(getActivity());
                mGameEngine.setInputController(new CompositeInputController(getView(), getHockeyActivity()));

                Player p1 = new Player(getView(),1);
                Player p2 = new Player(getView(),2);
                Puck puck = new Puck(getView(),p1,p2);
                Goal goal_1 = new Goal(getView(),puck,p1,p2,1);
                Goal goal_2 = new Goal(getView(),puck,p1,p2,2);
                ScoreBoard scoreBoard = new ScoreBoard(getView(), goal_1, goal_2);
                mGameEngine.addGameObject(p1);
                mGameEngine.addGameObject(p2);
                mGameEngine.addGameObject(puck);
                mGameEngine.addGameObject(goal_1);
                mGameEngine.addGameObject(goal_2);
                mGameEngine.addGameObject(scoreBoard);

                mGameEngine.startGame();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    InputManager inputManager = (InputManager) getActivity().getSystemService(Context.INPUT_SERVICE);
                    inputManager.registerInputDeviceListener(GameFragment.this, null);

                }
            }
        });
        Context context = view.getContext();
        SharedPreferences sharedPref = ((Activity)view.getContext()).getPreferences(Context.MODE_PRIVATE);
        mMap = sharedPref.getInt("saved_spinner2",0);
        Map(view,mMap);
    }


    private void Map(final View view,int choice){
        switch (choice){
            case 0:
                Drawable bg = view.getContext().getResources().getDrawable(R.drawable.hockeytable0);
                view.findViewById(R.id.gamebg).setBackground(bg);
                break;
            case 1:
                Drawable bg2 = view.getContext().getResources().getDrawable(R.drawable.neonhockeytable);
                view.findViewById(R.id.gamebg).setBackground(bg2);
                break;
            case 2:
                Drawable bg3 = view.getContext().getResources().getDrawable(R.drawable.airhockeysoccer);
                view.findViewById(R.id.gamebg).setBackground(bg3);
                break;

        }

    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_play_pause) {
            pauseGameAndShowPauseDialog();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGameEngine.isRunning()){
            pauseGameAndShowPauseDialog();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGameEngine.stopGame();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            InputManager inputManager = (InputManager) getActivity().getSystemService(Context.INPUT_SERVICE);
            inputManager.unregisterInputDeviceListener(this);
        }
    }

    @Override
    public boolean onBackPressed() {
        if (mGameEngine.isRunning()){
            pauseGameAndShowPauseDialog();
            return true;
        }
        return false;
    }

    private void pauseGameAndShowPauseDialog() {
        if (mGameEngine.isPaused()) {
            return;
        }
        mGameEngine.pauseGame();
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.pause_dialog_title)
                .setMessage(R.string.pause_dialog_message)
                .setPositiveButton(R.string.resume, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        mGameEngine.resumeGame();
                    }
                })
                .setNegativeButton(R.string.stop, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        mGameEngine.stopGame();
                        ((HockeyActivity)getActivity()).navigateBack();
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        mGameEngine.resumeGame();
                    }
                })
                .create()
                .show();
    }

    @Override
    public void onInputDeviceAdded(int deviceId) {

        }

    @Override
    public void onInputDeviceRemoved(int deviceId) {
        if (mGameEngine.isRunning()) {
            pauseGameAndShowPauseDialog();
        }
    }

    @Override
    public void onInputDeviceChanged(int deviceId) {

    }
}
