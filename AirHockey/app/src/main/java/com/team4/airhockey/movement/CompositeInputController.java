package com.team4.airhockey.movement;

import android.view.View;

import com.team4.airhockey.HockeyActivity;
import com.team4.airhockey.engine.InputController;

/**
 * Created by ryu24 on 2017-04-28.
 */

public class CompositeInputController extends InputController {

    private MoveToTouchInputController mP1MoveToInputController;
    private MoveToTouchInputController mP2MoveToInputController;

    private float mP2YOffset;

    public CompositeInputController(View view, HockeyActivity activity) {
        mP1MoveToInputController = new MoveToTouchInputController(view, 1);
        mP2MoveToInputController = new MoveToTouchInputController(view, 2);
        mP2YOffset = view.getHeight()/2f;
    }

    public void onPreUpdate() {
        //mIsFiring = mGamepadInputController.mIsFiring || mVJoystickInputController.mIsFiring;


        mLeftHorizontalPosition =  mP1MoveToInputController.targetX;
        mLeftVerticalPosition =  mP1MoveToInputController.targetY;
        mLeftIsTouching = mP1MoveToInputController.isTouching;

        mRightHorizontalPosition = mP2MoveToInputController.targetX;
        mRightVerticalPosition = mP2MoveToInputController.targetY + mP2YOffset;
        mRightIsTouching = mP2MoveToInputController.isTouching;


    }

    @Override
    public void onStart() {
        mP1MoveToInputController.onStart();
        mP2MoveToInputController.onStart();
    }

    @Override
    public void onStop() {
        mP1MoveToInputController.onStop();
        mP2MoveToInputController.onStop();
    }

    @Override
    public void onPause() {
        mP1MoveToInputController.onPause();
        mP2MoveToInputController.onPause();
    }

    @Override
    public void onResume() {
        mP1MoveToInputController.onResume();
        mP2MoveToInputController.onResume();
    }
}
