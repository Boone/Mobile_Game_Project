package com.team4.airhockey.Fragments;

import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.team4.airhockey.HockeyActivity;
import com.team4.airhockey.HockeyBaseFragment;
import com.team4.airhockey.R;

/**
 * Created by ryu24 on 2017-05-23.
 */



public class CustomizeFragment extends HockeyBaseFragment  {

    private Spinner spinner1, spinner2;

    Context context = null;
    SharedPreferences sharedPref = null;
    FrameLayout frameLayout;
    int mMap;
    int mPaddle;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_customize, container, false);
        return rootView;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){

        Drawable bg = view.getContext().getResources().getDrawable(R.drawable.hockeytable0);
        view.findViewById(R.id.customizebg).setBackground(bg);

//        addItemsOnSpinner2(view);

        context = getActivity();
        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);




        addListenerOnButton(view);
        addListenerOnSpinnerItemSelection(view);


        view.findViewById(R.id.btn_goback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HockeyActivity)getActivity()).mainMenu();
            }
        });
        Context context = view.getContext();
        SharedPreferences sharedPref = ((Activity)view.getContext()).getPreferences(Context.MODE_PRIVATE);
        mMap = sharedPref.getInt("saved_spinner2",0);
        mPaddle=sharedPref.getInt("saved_spinner1",0);
        Map(view,mMap);
        setSpinner1(mPaddle);
    }

    private void Map(final View view,int choice) {
        switch (choice) {
            case 0:
                Drawable bg = view.getContext().getResources().getDrawable(R.drawable.hockeytable0);
                view.findViewById(R.id.customizebg).setBackground(bg);
                spinner2.setSelection(0);
                break;
            case 1:
                Drawable bg2 = view.getContext().getResources().getDrawable(R.drawable.neonhockeytable);
                view.findViewById(R.id.customizebg).setBackground(bg2);
                spinner2.setSelection(1);
                break;
            case 2:
                Drawable bg3 = view.getContext().getResources().getDrawable(R.drawable.airhockeysoccer);
                view.findViewById(R.id.customizebg).setBackground(bg3);
                spinner2.setSelection(2);
                break;

        }
    }

    private void setSpinner1(int choice){
        switch (choice){
            case 0:
                spinner1.setSelection(0);
                break;
            case 1:
                spinner1.setSelection(1);
                break;
            case 2:
                spinner1.setSelection(2);
                break;
            case 3:
                spinner1.setSelection(3);
                break;
            case 4:
                spinner1.setSelection(4);
                break;
            case 5:
                spinner1.setSelection(5);
                break;
            default:
                spinner1.setSelection(0);
                break;


        }

    }


    public void addListenerOnSpinnerItemSelection(View view) {
        spinner1 = (Spinner) view.findViewById(R.id.spinner1);
        spinner2 = (Spinner) view.findViewById(R.id.spinner2);
//        spinner1.setOnItemSelectedListener(new CustomOnItemSelectedListener());

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("saved_spinner1",spinner1.getSelectedItemPosition());

                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("saved_spinner2",spinner2.getSelectedItemPosition());

                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    // get the selected dropdown list value
    public void addListenerOnButton(View view) {

        spinner1 = (Spinner) view.findViewById(R.id.spinner1);
        spinner2 = (Spinner) view.findViewById(R.id.spinner2);



    }


}
