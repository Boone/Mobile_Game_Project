package com.team4.airhockey.movement;

import android.view.MotionEvent;
import android.view.View;

import com.team4.airhockey.R;
import com.team4.airhockey.engine.InputController;

/**
 * Created by ryu24 on 2017-05-18.
 */

public class MoveToTouchInputController extends InputController {

    public float targetX;
    public float targetY;
    public boolean isTouching = false;

    public MoveToTouchInputController(View view, int player) {

        if (player == 1) {
            view.findViewById(R.id.vjoystick_player1).setOnTouchListener(new MoveToTouchInputController.MoveToTouchListener());
        }
        else if (player == 2){
            view.findViewById(R.id.vjoystick_player2).setOnTouchListener(new MoveToTouchInputController.MoveToTouchListener());
        }

        double pixelFactor = view.getHeight() / 400d;

    }

    private class MoveToTouchListener implements View.OnTouchListener {


        public MoveToTouchListener() {
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            int action = event.getActionMasked();
            if (action == MotionEvent.ACTION_DOWN) {
                targetX = event.getX(0);
                targetY = event.getY(0);

                isTouching=true;
                
            } else if (action == MotionEvent.ACTION_UP) {
                isTouching = false;
            } else if (action == MotionEvent.ACTION_MOVE&&isTouching) {
                isTouching = true;
                targetX = event.getX(0);
                targetY = event.getY(0);
            }

            return true;
        }
    }
}
