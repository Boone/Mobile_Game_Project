package com.team4.airhockey.Fragments;

import com.team4.airhockey.R;
import com.team4.airhockey.HockeyActivity;
import com.team4.airhockey.HockeyBaseFragment;
import com.team4.airhockey.engine.MusicManager;
import com.team4.airhockey.Fragments.SettingsFragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.InputDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * Created by ryu24 on 2017-04-24.
 */

public class MainMenuFragment extends HockeyBaseFragment {
    public MainMenuFragment() {}

    private static final String PREF_SHOULD_DISPLAY_GAMEPAD_HELP = "com.example.yass.display.gamepad.help.boolean";
    int mMap;
    int mMusicOn;
    //private SettingsFragment settingsFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_menu, container, false);
        return rootView;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btn_1v1start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HockeyActivity) getActivity()).startGame();
            }
        });
        view.findViewById(R.id.btn_settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HockeyActivity) getActivity()).settings();
            }
        });
        view.findViewById(R.id.btn_customize).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HockeyActivity) getActivity()).customize();
            }
        });
        view.findViewById(R.id.btn_exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
        });


       // mMusicOn = settingsFragment.musicOn;



        Context context = view.getContext();
        SharedPreferences sharedPref = ((Activity)view.getContext()).getPreferences(Context.MODE_PRIVATE);
        mMusicOn = sharedPref.getInt("musicOn",1);
        if (mMusicOn == 1){
            MusicManager.getInstance().play((getActivity()), R.raw.red_doors);
        }
        else{
        }


        mMap = sharedPref.getInt("saved_spinner2",0);
        Map(view,mMap);


    }

    private void Map(final View view,int choice){
        switch (choice){
            case 0:
                Drawable bg = view.getContext().getResources().getDrawable(R.drawable.hockeytable0);
                view.findViewById(R.id.menubg).setBackground(bg);
                break;
            case 1:
                Drawable bg2 = view.getContext().getResources().getDrawable(R.drawable.neonhockeytable);
                view.findViewById(R.id.menubg).setBackground(bg2);
                break;
            case 2:
                Drawable bg3 = view.getContext().getResources().getDrawable(R.drawable.airhockeysoccer);
                view.findViewById(R.id.menubg).setBackground(bg3);
                break;

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (isGameControllerConnected() && shouldDisplayGamepadHelp()) {
            displayGamepadHelp();
            // Do not show the dialog again
            PreferenceManager.getDefaultSharedPreferences(getActivity())
                    .edit()
                    .putBoolean(PREF_SHOULD_DISPLAY_GAMEPAD_HELP, false)
                    .commit();
        }
    }

    private void displayGamepadHelp() {
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.gamepad_help_title)
                .setMessage(R.string.gamepad_help_message)
                .create()
                .show();
    }

    private boolean shouldDisplayGamepadHelp() {
        return PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getBoolean(PREF_SHOULD_DISPLAY_GAMEPAD_HELP, true);
    }

    public boolean isGameControllerConnected() {
        int[] deviceIds = InputDevice.getDeviceIds();
        for (int deviceId : deviceIds) {
            InputDevice dev = InputDevice.getDevice(deviceId);
            int sources = dev.getSources();
            if (((sources & InputDevice.SOURCE_GAMEPAD) == InputDevice.SOURCE_GAMEPAD) ||
                    ((sources & InputDevice.SOURCE_JOYSTICK) == InputDevice.SOURCE_JOYSTICK)) {
                return true;
            }
        }
        return false;
    }
}


