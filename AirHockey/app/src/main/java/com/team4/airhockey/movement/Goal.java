package com.team4.airhockey.movement;


import com.team4.airhockey.R;
import com.team4.airhockey.engine.GameEngine;
import com.team4.airhockey.engine.GameObject;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.shapes.Shape;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;


/**
 * Created by ryu24 on 2017-05-12.
 */

public class Goal extends GameObject {

    private Puck mPuck;
    private int mGoal;
    Player mPlayer1;
    Player mPlayer2;
    private int mMaxX;
    private int mMaxY;
    public int score1;
    public int score2;
    public String scoreString1;
    public String scoreString2;
    private double mPixelFactor;
    private final View mView;
    public Rect mGoalShape = new Rect();
    LinearLayout coloredShape;

    public Goal(View view,Puck puck,Player player1,Player player2, int goal){
        mView = view;
        mPuck = puck;
        mPixelFactor = view.getHeight() / 400d;
        mMaxX = view.getWidth() - view.getPaddingLeft() - view.getPaddingRight();
        mMaxY = view.getHeight() - view.getPaddingTop() - view.getPaddingBottom();
        mGoal=goal;
        mPlayer1 = player1;
        mPlayer2 = player2;

        ///
//        mPixelFactor = view.getHeight() / 400d;
//        mMaxX = view.getWidth() - view.getPaddingLeft() - view.getPaddingRight();
//        mMaxY = view.getHeight() - view.getPaddingTop() - view.getPaddingBottom();
//        mPuck = new ImageView(view.getContext());
//        Drawable puckDrawable = view.getContext().getResources().getDrawable(R.drawable.puck);
//
//        mPuck.setLayoutParams(new ViewGroup.LayoutParams(
//                (int) (puckDrawable.getIntrinsicWidth() * mPixelFactor),
//                (int) (puckDrawable.getIntrinsicHeight() * mPixelFactor)));
//        mPuck.setImageDrawable(puckDrawable);
//        ((FrameLayout) view).addView(mPuck);
//        mMaxX -= (puckDrawable.getIntrinsicWidth()*mPixelFactor);
//        mMaxY -= (puckDrawable.getIntrinsicHeight()*mPixelFactor);
//        mRadius = (puckDrawable.getIntrinsicWidth() * mPixelFactor)/2;
        ///



        coloredShape = new LinearLayout(view.getContext());


        coloredShape.setLayoutParams(new ViewGroup.LayoutParams(
                (int) mMaxX / 3,
                50
        ));

        ((FrameLayout) view).addView(coloredShape);


        if (goal == 1)
        {
            mGoalShape.bottom = 0;
            mGoalShape.top = 0;
        }
        else
        {
            mGoalShape.bottom = mMaxY;
            mGoalShape.top = mMaxY-50;
        }
        mGoalShape.left = mMaxX/3;
        mGoalShape.right=mMaxX-mGoalShape.left;

    }

    @Override
    public void startGame() {
        score1=0;
        score2=0;
        scoreString1 = Integer.toString(score1);
        scoreString2 = Integer.toString(score2);
    }

    @Override
    public void onUpdate(long elapsedMillis, GameEngine gameEngine) {
      boolean collision = checkCollisionWithPuckTop(mPuck, mGoal);

        if (collision){

            if (mGoal == 1) {
                mPlayer1.mPositionX= mMaxX / 2 - 75;
                mPlayer1.mPositionY=mMaxY / 2-500;
                mPlayer2.mPositionX= mMaxX / 2 - 75;
                mPlayer2.mPositionY=mMaxY / 2 + 500;
                mPuck.mPositionX = mMaxX / 2 - 40;
                mPuck.mPositionY = (mMaxY / 2) - 300;
                score1 +=1;
                scoreString1 = Integer.toString(score1);
            }
            if (mGoal == 2)
            {
                mPlayer1.mPositionX= mMaxX/2 - 75;
                mPlayer1.mPositionY=mMaxY/2-500;
                mPlayer2.mPositionX= mMaxX/2 - 75;
                mPlayer2.mPositionY=mMaxY/2+500;
                mPuck.mPositionX = mMaxX / 2 - 40;
                mPuck.mPositionY = (mMaxY / 2) + 300;
                score2+=1;
                scoreString2 = Integer.toString(score2);
            }

        }
    }

    public void onDraw(){
        coloredShape.animate().translationX((int) mGoalShape.left).translationY((int) mGoalShape.top)
                .setDuration(1)
                .start();
    }

    public boolean checkCollisionWithPuckTop(Puck puck, int goal){
        mPuck = puck;
        mGoal = goal;

        if (mGoal == 1){
        if (mPuck.mPositionX > mGoalShape.left && mPuck.mPositionX < mGoalShape.right && mPuck.mPositionY < 80) {
            mPuck.mXVelocity = 0;
            mPuck.mYVelocity = 0;

            return true;
        }
        }

        if (mGoal == 2){
            if (mPuck.mPositionX > mGoalShape.left && mPuck.mPositionX < mGoalShape.right && mPuck.mPositionY > mMaxY-150){
            mPuck.mXVelocity = 0;
            mPuck.mYVelocity = 0;

            return true;}

        }

        return false;
    }
}
