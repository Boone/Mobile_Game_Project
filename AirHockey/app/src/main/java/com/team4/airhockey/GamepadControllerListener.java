package com.team4.airhockey;

import android.view.KeyEvent;
import android.view.MotionEvent;

/**
 * Created by ryu24 on 2017-04-28.
 */

public interface GamepadControllerListener {
    boolean dispatchGenericMotionEvent(MotionEvent event);

    boolean dispatchKeyEvent(KeyEvent event);
}
