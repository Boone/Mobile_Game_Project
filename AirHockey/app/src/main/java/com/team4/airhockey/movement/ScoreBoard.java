package com.team4.airhockey.movement;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.team4.airhockey.HockeyActivity;
import com.team4.airhockey.R;

import com.team4.airhockey.engine.GameEngine;
import com.team4.airhockey.engine.GameObject;

/**
 * Created by ryu24 on 2017-05-15.
 */

public class ScoreBoard extends GameObject {




    LinearLayout coloredShape;
    TextView scoreText1;
    TextView scoreText2;
    TextView Win;
    TextView Loss;
    private double mPixelFactor;
    private int mMaxX;
    private int mMaxY;
    private int stringGoalToInt1;
    private int stringGoalToInt2;
    private boolean win;
    View mView;
    Goal mGoal;
    Goal mGoal2;
    public Rect mScoreBoard = new Rect();


    //private final View mView;

    public ScoreBoard(View view, Goal goal, Goal goal2){
        mMaxX = view.getWidth() - view.getPaddingLeft() - view.getPaddingRight();
        mMaxY = view.getHeight() - view.getPaddingTop() - view.getPaddingBottom();
        mPixelFactor = view.getHeight() / 400d;
        mGoal = goal;
        mGoal2 = goal2;
        mView = view;

        coloredShape = new LinearLayout(view.getContext());


        coloredShape.setLayoutParams(new ViewGroup.LayoutParams(
                (int) mMaxX/3 ,
                500
        ));

        ((FrameLayout) view).addView(coloredShape);

        mScoreBoard.right=mMaxX;
        mScoreBoard.left =mMaxX-200;
        mScoreBoard.top=mMaxX/2+150;
        mScoreBoard.bottom=mMaxX;

        RotateAnimation rotate=(RotateAnimation) AnimationUtils.loadAnimation(view.getContext(),R.anim.rotateanimation);

        scoreText1 = new TextView(view.getContext());

        //scoreText1.setTextColor(0xCC000000);

        scoreText1.setLayoutParams(new ViewGroup.LayoutParams(

                (int) mMaxX/2,
                300
        ));
        scoreText1.setText("0c0");
        scoreText1.setTextSize(32);
        //scoreText1.setAnimation(rotate);

        ((FrameLayout) view).addView(scoreText1);

        scoreText2 = new TextView((view.getContext()));

        //scoreText2.setTextColor(0xCC000000);

        scoreText2.setLayoutParams(new ViewGroup.LayoutParams(

                (int) mMaxX,
                300
        ));

        ((FrameLayout)view).addView(scoreText2);
        scoreText2.setText("0d0");
        scoreText2.setTextSize(32);

        Win = new TextView(view.getContext());
        Loss = new TextView(view.getContext());
        Win.setTextSize(32);
        Loss.setTextSize(32);
        Win.setText("Winner!");
        Loss.setText("Loser!");
            ((FrameLayout) mView).addView(Win);
            ((FrameLayout) mView).addView(Loss);



    }

    public boolean winCondition(){
        if (mGoal.score1 == 7 && !win){

            win = true;
        }
        if (mGoal2.score2 == 7&& !win){
            win = true;
        }
        return  win;
    }

    public void startGame(){}

    private  int timer;

    public void onUpdate(long elapsedMillis, GameEngine gameEngine){
    //winCondition();
        if (winCondition()){
            timer+=elapsedMillis;
            if (timer == 3000){
                ((HockeyActivity)mView.getContext()).mainMenu();
                gameEngine.stopGame();
            }
        }
    }


    public void onDraw() {

        if (mGoal.scoreString1 != null) {
            //stringGoalToInt1 = Integer.parseInt(mGoal.scoreString1);
            if (scoreText1.getText() != mGoal.scoreString1){
                scoreText1.setText(mGoal.scoreString1);}
        }
        if (mGoal2.scoreString2 != null) {
            //stringGoalToInt2 = Integer.parseInt(mGoal2.scoreString2);
            if (scoreText2.getText() != mGoal2.scoreString2) {
                scoreText2.setText(mGoal2.scoreString2);
            }
        }


        scoreText1.setPivotY(0.5f);
        scoreText1.setPivotX(0.5f);
        scoreText2.setPivotY(0.5f);
        scoreText2.setPivotX(0.5f);
        Win.setPivotX(0.5f);
        Win.setPivotY(0.5f);
        Loss.setPivotX(0.5f);
        Loss.setPivotY(0.5f);
        scoreText1.animate()
                .translationX((int) mMaxX-110).translationY((int)mMaxY/1.95f)
                .setDuration(1)
                .start();
        scoreText2.animate()
                .rotation(180f)
                .translationX((int) mMaxX-60).translationY((int)mMaxY-(mMaxY/1.95f))
                .setDuration(1)
                .start();
        coloredShape.animate().translationX((int) mScoreBoard.left).translationY((int) mScoreBoard.top)
                .setDuration(1)
                .start();
        Win.animate()
                .translationX((int) mMaxX+500).translationY((int)mMaxY/4)
                .setDuration(1)
                .start();
        Loss.animate()
                .rotation(180f)
                .translationX((int) mMaxX+500).translationY((int)mMaxY-(mMaxY/4))
                .setDuration(1)
                .start();
        if (winCondition()&&mGoal.score1 == 7){Win.animate()
                .translationX((int) mMaxX/2).translationY((int)mMaxY/1.5f)
                .setDuration(1)
                .start();
            Loss.animate()
                    .rotation(180f)
                    .translationX((int) mMaxX/2).translationY((int)mMaxY/3.5f)
                    .setDuration(1)
                    .start();}
        if (winCondition()&&mGoal2.score2 == 7){Win.animate()
                .rotation(180f)
                .translationX((int) mMaxX/2).translationY((int)mMaxY/4)
                .setDuration(1)
                .start();
            Loss.animate()
                    .rotation(0f)
                    .translationX((int) mMaxX/2).translationY((int)mMaxY-(mMaxY/4))
                    .setDuration(1)
                    .start();}
    }
}
