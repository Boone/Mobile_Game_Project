package com.team4.airhockey;

//import android.support.v4.app.Fragment;
import android.app.Fragment;


/**
 * A placeholder fragment containing a simple view.
 */
public class HockeyBaseFragment extends Fragment {

        public boolean onBackPressed(){return false;}

        protected HockeyActivity getHockeyActivity() {
                return (HockeyActivity) getActivity();
        }
}