package com.team4.airhockey.movement;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.team4.airhockey.R;
import com.team4.airhockey.engine.GameEngine;
import com.team4.airhockey.engine.GameObject;
import com.team4.airhockey.engine.InputController;


/**
 * Created by ryu24 on 2017-04-25.
 */


public class Player extends GameObject {

    private static final int INITIAL_BULLET_POOL_AMOUNT = 6;
    private static final long TIME_BETWEEN_BULLETS = 250;
    private final View mView;

    //List<Bullet> mBullets = new ArrayList<Bullet>();


    private int mMaxX;
    private int mMaxY;


    double mPositionX;
    double mPositionY;
    double mRadius;
    double mVelocity;
    int mCustom;



    double mSpeedFactor;
    private double mPixelFactor;

    private int mPlayer;
    public ImageView mPaddle;

    public Player(final View view, int player) {

        Context context = view.getContext();
        SharedPreferences sharedPref = ((Activity)view.getContext()).getPreferences(Context.MODE_PRIVATE);
        mView = view;
        // We read the size of the view
        mPixelFactor = view.getHeight() / 400d;
        mMaxX = view.getWidth() - view.getPaddingLeft() - view.getPaddingRight();
        mMaxY = view.getHeight() - view.getPaddingTop() - view.getPaddingBottom();
        mSpeedFactor = mPixelFactor * 0.001d / 1000d; // We want to move at 100px per second on a 400px tall screen
        // We create an image view and add it to the view
        mCustom = sharedPref.getInt("saved_spinner1", 0);

        mPlayer = player;

            PaddleImage(view,mCustom);


        }


    private void PaddleImage(final View view,int choice){

        mPaddle = new ImageView(view.getContext());

        Drawable paddleDrawable = null;
        if(mPlayer == 1)
        {
            switch (choice) {
                case 0:
                    paddleDrawable = view.getContext().getResources().getDrawable(R.drawable.paddle_red);
                    break;
                case 4:
                    paddleDrawable = view.getContext().getResources().getDrawable(R.drawable.pancake);
                    break;
                case 1:
                    paddleDrawable = view.getContext().getResources().getDrawable(R.drawable.paddle_blk);
                    break;
                case 2:
                    paddleDrawable = view.getContext().getResources().getDrawable(R.drawable.paddle_purple);
                    break;
                case 3:
                    paddleDrawable = view.getContext().getResources().getDrawable(R.drawable.paddle_orange);
                    break;
                default:
                    paddleDrawable = view.getContext().getResources().getDrawable(R.drawable.paddle_red);
            }
        }
        else {
            switch (choice) {
                case 0:
                    paddleDrawable = view.getContext().getResources().getDrawable(R.drawable.paddle_blue);
                    break;
                case 4:
                    paddleDrawable = view.getContext().getResources().getDrawable(R.drawable.pancake);
                    break;
                case 1:
                    paddleDrawable = view.getContext().getResources().getDrawable(R.drawable.paddle_white);
                    break;
                case 2:
                    paddleDrawable = view.getContext().getResources().getDrawable(R.drawable.paddle_yellow);
                    break;
                case 3:
                    paddleDrawable = view.getContext().getResources().getDrawable(R.drawable.paddle_green);
                    break;
                default:
                    paddleDrawable = view.getContext().getResources().getDrawable(R.drawable.paddle_blue);

            }
        }

        if (paddleDrawable != null){
            mPaddle.setLayoutParams(new ViewGroup.LayoutParams(
                    (int) (paddleDrawable.getIntrinsicWidth() * mPixelFactor),
                    (int) (paddleDrawable.getIntrinsicHeight() * mPixelFactor)));
            mPaddle.setImageDrawable(paddleDrawable);
            ((FrameLayout) view).addView(mPaddle);
            mMaxX -= (paddleDrawable.getIntrinsicWidth()*mPixelFactor);
            mMaxY -= (paddleDrawable.getIntrinsicHeight()*mPixelFactor);
            mRadius = (paddleDrawable.getIntrinsicWidth() * mPixelFactor)/2;
        }
    }





    @Override
    public void startGame() {
        if (mPlayer == 1)
        {
        mPositionX = mMaxX/2;
        mPositionY = (mMaxY/2)-500;
    }
    else
        {
            mPositionX = mMaxX/2;
            mPositionY = (mMaxY/2)+500;
        }
    }


    @Override
    public void onUpdate(long elapsedMillis, GameEngine gameEngine) {
        // Get the info from the inputController
        updatePosition(elapsedMillis, gameEngine.mInputController);
        //check collision




        //on collision
    }



    private void updatePosition(long elapsedMillis, InputController inputController) {

        if (mPlayer == 1 && inputController.mLeftIsTouching) {
            double mod = mPositionX < (inputController.mLeftHorizontalPosition - mRadius) ? 1 : -1;
            mPositionX += 1*mod * elapsedMillis;
            //mPositionX -= mRadius;

            mod = mPositionY < (inputController.mLeftVerticalPosition - mRadius) ? 1 : -1;
            mPositionY += 1*mod * elapsedMillis;
            //mPositionY -= mRadius;
        }
        else if (mPlayer == 2 && inputController.mRightIsTouching){
            double val = inputController.mRightHorizontalPosition;
            double mod = mPositionX < (val - mRadius) ? 1 : -1;
            mPositionX += 1*mod * elapsedMillis;
            //mPositionX -= mRadius;

            double valY = inputController.mRightVerticalPosition - mRadius;
            mod = mPositionY < (valY) ? 1 : -1;
            mPositionY += 1*mod * elapsedMillis;
            //mPositionY -= mRadius;
        }

        if (mPositionX < 0) {
            mPositionX = 0;
        }
        if (mPositionX > mMaxX) {
            mPositionX = mMaxX;
        }

        if (mPlayer == 1) {
            if (mPositionY < 0)
            {
                mPositionY = 0;
            }
            if (mPositionY > ((mMaxY / 2) - (mPaddle.getHeight()/2))) {
                mPositionY = ((mMaxY / 2) - (mPaddle.getHeight()/2));
            }
        }
        else{
            if (mPositionY <  ((mMaxY/2) + (mPaddle.getHeight()/2)))
            {
                mPositionY =((mMaxY/2) + (mPaddle.getHeight()/2));
            }
            if(mPositionY > mMaxY){
                mPositionY = mMaxY;
            }

        }

//        if (mPlayer == 1) {
//            mPositionX += mSpeedFactor * inputController.mLeftHorizontalFactor * elapsedMillis;
//            if (mPositionX > inputController.mLeftHorizontalFactor){
//                mPositionX +=0;
//            }
//        }
//        else {
//            mPositionX += mSpeedFactor * inputController.mRightHorizontalFactor * elapsedMillis;
//        }



//        if (mPlayer == 1) {
//            mPositionY += mSpeedFactor * inputController.mLeftVerticalFactor * elapsedMillis;
//        }
//        else {
//            mPositionY += mSpeedFactor * inputController.mRightVerticalFactor * elapsedMillis;
//        }






    }

    @Override
    public void onDraw() {
        mPaddle.animate().translationX((int) mPositionX).translationY((int) mPositionY)
                .setDuration(1)
                .start();
    }
}
