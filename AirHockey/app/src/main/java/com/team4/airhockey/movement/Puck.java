package com.team4.airhockey.movement;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.lang.Math;
import com.team4.airhockey.R;
import com.team4.airhockey.engine.GameEngine;
import com.team4.airhockey.engine.GameObject;
import com.team4.airhockey.engine.InputController;

import static java.lang.Math.atan;

/**
 * Created by ryu24 on 2017-05-08.
 */



public class Puck extends GameObject{

    public ImageView mPuck;
    private int mMaxX;
    private int mMaxY;
    private final View mView;


    float mPositionX;
    float mPositionY;
    double mRadius;
    double mDistance;
    double mDistanceX;
    double mDistanceY;
    double mAngle;
    float mXVelocity;
    float mYVelocity;
    int mMap;

    private Player mP1;
    private Player mP2;


    private double mPixelFactor;


    public Puck(final View view, Player p1, Player p2){
        mView = view;
        mP1 = p1;
        mP2 = p2;
        SharedPreferences sharedPref = ((Activity)view.getContext()).getPreferences(Context.MODE_PRIVATE);
        mMap = sharedPref.getInt("saved_spinner2", 0);

        mPixelFactor = view.getHeight() / 400d;
        mMaxX = view.getWidth() - view.getPaddingLeft() - view.getPaddingRight();
        mMaxY = view.getHeight() - view.getPaddingTop() - view.getPaddingBottom();
        map(view,mMap);


//        getmPuck.setLayoutParams(new ViewGroup.LayoutParams(
//                (int)
//                        (int)
//        ));








    }

    private  void  map(View view,int choice){
        mPuck = new ImageView(view.getContext());

        Drawable puckDrawable = null;


        switch (choice){
            case 0:
                 puckDrawable = view.getContext().getResources().getDrawable(R.drawable.puck);
                break;
            case 1:
                 puckDrawable = view.getContext().getResources().getDrawable(R.drawable.neon_puck);
                break;
            case 2:
                 puckDrawable = view.getContext().getResources().getDrawable(R.drawable.ball_puck);
                break;
            default:
                puckDrawable = view.getContext().getResources().getDrawable(R.drawable.puck);
                break;


        }


        mPuck.setLayoutParams(new ViewGroup.LayoutParams(
                (int) (puckDrawable.getIntrinsicWidth() * mPixelFactor),
                (int) (puckDrawable.getIntrinsicHeight() * mPixelFactor)));
        mPuck.setImageDrawable(puckDrawable);
        ((FrameLayout) view).addView(mPuck);
        mMaxX -= (puckDrawable.getIntrinsicWidth()*mPixelFactor);
        mMaxY -= (puckDrawable.getIntrinsicHeight()*mPixelFactor);
        mRadius = (puckDrawable.getIntrinsicWidth() * mPixelFactor)/2;

    }


    public void startGame(){
        mPositionX=mMaxX/2;
        mPositionY=mMaxY/2;
    }

    private void updatePosition(long elapsedMillis, InputController inputController) {
        //check collision
        boolean colp1 = checkCollisionWithPlayer(mP1);
        boolean colp2 = checkCollisionWithPlayer(mP2);
        mPositionX += mXVelocity;
        mPositionY += mYVelocity;
        //on collision
        if (colp1) {

            //step 1 check force

            //mP1.mSpeedFactor

            //step 2 check direction
            mAngle = atan(mDistanceY / mDistanceX) * (180/Math.PI);

            //step 3 move puck outside of collision
            //step 4 apply force to puck
            if (mAngle < 11 && mP1.mPositionX < mPositionX && mP1.mPositionY < mPositionY) { mYVelocity+=0.000; mXVelocity +=0.001; mPositionX+=5; } // +X
            if (mAngle < 11 && mP1.mPositionX < mPositionX && mP1.mPositionY > mPositionY) { mYVelocity+=0.000; mXVelocity +=0.001; mPositionX+=5; } // +X
            if (mAngle < 11 && mP1.mPositionX > mPositionX && mP1.mPositionY > mPositionY) { mYVelocity+=0.000; mXVelocity -=0.001; mPositionX-=5; } // -X
            if (mAngle < 11 && mP1.mPositionX > mPositionX && mP1.mPositionY < mPositionY) { mYVelocity+=0.000; mXVelocity -=0.001; mPositionX-=5; } // -X
            if (mAngle < 11 && mP1.mPositionY < mPositionY && mP1.mPositionX < mPositionX) { mYVelocity+=0.001; mXVelocity +=0.000; mPositionY+=5; } // +Y
            if (mAngle < 11 && mP1.mPositionY < mPositionY && mP1.mPositionX > mPositionX) { mYVelocity+=0.001; mXVelocity +=0.000; mPositionY+=5; } // +Y
            if (mAngle < 11 && mP1.mPositionY > mPositionY && mP1.mPositionX > mPositionX) { mYVelocity-=0.001; mXVelocity +=0.000; mPositionY-=5; } // -Y
            if (mAngle < 11 && mP1.mPositionY > mPositionY && mP1.mPositionX < mPositionX) { mYVelocity-=0.001; mXVelocity +=0.000; mPositionY-=5; } // -Y

            if (mAngle > 10 && mAngle < 81 && mP1.mPositionX > mPositionX && mP1.mPositionY > mPositionY) {mXVelocity -=0.001; mYVelocity-=0.001;mPositionY -=5;mPositionX -=5; } // -X -Y
            if (mAngle > 10 && mAngle < 81 && mP1.mPositionX < mPositionX && mP1.mPositionY < mPositionY) {mXVelocity +=0.001; mYVelocity+=0.001;mPositionY +=5;mPositionX +=5; } // +X +Y
            if (mAngle > 10 && mAngle < 81 && mP1.mPositionX > mPositionX && mP1.mPositionY < mPositionY) {mXVelocity -=0.001; mYVelocity+=0.001;mPositionY +=5;mPositionX -=5; } // -X +Y
            if (mAngle > 10 && mAngle < 81 && mP1.mPositionX < mPositionX && mP1.mPositionY > mPositionY) {mXVelocity +=0.001; mYVelocity-=0.001;mPositionY -=5;mPositionX +=5; } // +X -Y
            
            if (mAngle > 80 && mAngle < 91 && mP1.mPositionX < mPositionX && mP1.mPositionY < mPositionY) {mYVelocity+=0.000; mXVelocity +=0.001; mPositionX+=5; } // +X
            if (mAngle > 80 && mAngle < 91 && mP1.mPositionX < mPositionX && mP1.mPositionY > mPositionY) {mYVelocity+=0.000; mXVelocity +=0.001; mPositionX+=5; } // +X
            if (mAngle > 80 && mAngle < 91 && mP1.mPositionX > mPositionX && mP1.mPositionY > mPositionY) {mYVelocity+=0.000; mXVelocity -=0.001; mPositionX-=5; } // -X
            if (mAngle > 80 && mAngle < 91 && mP1.mPositionX > mPositionX && mP1.mPositionY < mPositionY) {mYVelocity+=0.000; mXVelocity -=0.001; mPositionX-=5; } // -X
            if (mAngle > 80 && mAngle < 91 && mP1.mPositionY < mPositionY && mP1.mPositionX < mPositionX) {mYVelocity+=0.001; mXVelocity +=0.000; mPositionY+=5; } // +Y
            if (mAngle > 80 && mAngle < 91 && mP1.mPositionY < mPositionY && mP1.mPositionX > mPositionX) {mYVelocity+=0.001; mXVelocity +=0.000; mPositionY+=5; } // +Y
            if (mAngle > 80 && mAngle < 91 && mP1.mPositionY > mPositionY && mP1.mPositionX > mPositionX) {mYVelocity-=0.001; mXVelocity +=0.000; mPositionY-=5; } // -Y
            if (mAngle > 80 && mAngle < 91 && mP1.mPositionY > mPositionY && mP1.mPositionX < mPositionX) {mYVelocity-=0.001; mXVelocity +=0.000; mPositionY-=5; } // -Y



            if (mYVelocity >= 0.008) {
                mYVelocity = mYVelocity-0.001f;
            }
            if (mXVelocity >= 0.008) {
                mXVelocity = mXVelocity-0.001f;
            }
        }

        if (colp2){

            mAngle = atan(mDistanceY/mDistanceX)* (180/Math.PI);

            if (mAngle < 11 && mP2.mPositionX < mPositionX && mP2.mPositionY < mPositionY) { mYVelocity+=0.000; mXVelocity +=0.001; mPositionX+=5; } // +X
            if (mAngle < 11 && mP2.mPositionX < mPositionX && mP2.mPositionY > mPositionY) { mYVelocity+=0.000; mXVelocity +=0.001; mPositionX+=5; } // +X
            if (mAngle < 11 && mP2.mPositionX < mPositionX && mP2.mPositionY == mPositionY) { mYVelocity+=0.000; mXVelocity +=0.001; mPositionX+=5; } // +X
            if (mAngle < 11 && mP2.mPositionX > mPositionX && mP2.mPositionY > mPositionY) { mYVelocity+=0.000; mXVelocity -=0.001; mPositionX-=5; } // -X
            if (mAngle < 11 && mP2.mPositionX > mPositionX && mP2.mPositionY < mPositionY) { mYVelocity+=0.000; mXVelocity -=0.001; mPositionX-=5; } // -X
            if (mAngle < 11 && mP2.mPositionX > mPositionX && mP2.mPositionY == mPositionY) { mYVelocity+=0.000; mXVelocity -=0.001; mPositionX-=5; } // -X
            if (mAngle < 11 && mP2.mPositionY < mPositionY && mP2.mPositionX < mPositionX) { mYVelocity+=0.001; mXVelocity +=0.000; mPositionY+=5; } // +Y
            if (mAngle < 11 && mP2.mPositionY < mPositionY && mP2.mPositionX > mPositionX) { mYVelocity+=0.001; mXVelocity +=0.000; mPositionY+=5; } // +Y
            if (mAngle < 11 && mP2.mPositionY < mPositionY && mP2.mPositionX == mPositionX) { mYVelocity+=0.001; mXVelocity +=0.000; mPositionY+=5; } // +Y
            if (mAngle < 11 && mP2.mPositionY > mPositionY && mP2.mPositionX > mPositionX) { mYVelocity-=0.001; mXVelocity +=0.000; mPositionY-=5; } // -Y
            if (mAngle < 11 && mP2.mPositionY > mPositionY && mP2.mPositionX < mPositionX) { mYVelocity-=0.001; mXVelocity +=0.000; mPositionY-=5; } // -Y
            if (mAngle < 11 && mP2.mPositionY > mPositionY && mP2.mPositionX == mPositionX) { mYVelocity-=0.001; mXVelocity +=0.000; mPositionY-=5; } // -Y

            if (mAngle > 10 && mAngle < 81 && mP2.mPositionX > mPositionX && mP2.mPositionY > mPositionY) {mXVelocity -=0.001; mYVelocity-=0.001;mPositionY -=5;mPositionX -=5; } // -X -Y
            if (mAngle > 10 && mAngle < 81 && mP2.mPositionX < mPositionX && mP2.mPositionY < mPositionY) {mXVelocity +=0.001; mYVelocity+=0.001;mPositionY +=5;mPositionX +=5; } // +X +Y
            if (mAngle > 10 && mAngle < 81 && mP2.mPositionX > mPositionX && mP2.mPositionY < mPositionY) {mXVelocity -=0.001; mYVelocity+=0.001;mPositionY +=5;mPositionX -=5; } // -X +Y
            if (mAngle > 10 && mAngle < 81 && mP2.mPositionX < mPositionX && mP2.mPositionY > mPositionY) {mXVelocity +=0.001; mYVelocity-=0.001;mPositionY -=5;mPositionX +=5; } // +X -Y

            if (mAngle > 80 && mAngle < 91 && mP2.mPositionX < mPositionX && mP2.mPositionY < mPositionY) {mYVelocity+=0.000; mXVelocity +=0.001; mPositionX+=5; } // +X
            if (mAngle > 80 && mAngle < 91 && mP2.mPositionX < mPositionX && mP2.mPositionY > mPositionY) {mYVelocity+=0.000; mXVelocity +=0.001; mPositionX+=5; } // +X
            if (mAngle > 80 && mAngle < 91 && mP2.mPositionX < mPositionX && mP2.mPositionY == mPositionY) {mYVelocity+=0.000; mXVelocity +=0.001; mPositionX+=5; } // +X
            if (mAngle > 80 && mAngle < 91 && mP2.mPositionX > mPositionX && mP2.mPositionY > mPositionY) {mYVelocity+=0.000; mXVelocity -=0.001; mPositionX-=5; } // -X
            if (mAngle > 80 && mAngle < 91 && mP2.mPositionX > mPositionX && mP2.mPositionY < mPositionY) {mYVelocity+=0.000; mXVelocity -=0.001; mPositionX-=5; } // -X
            if (mAngle > 80 && mAngle < 91 && mP2.mPositionX > mPositionX && mP2.mPositionY == mPositionY) {mYVelocity+=0.000; mXVelocity -=0.001; mPositionX-=5; } // -X
            if (mAngle > 80 && mAngle < 91 && mP2.mPositionY < mPositionY && mP2.mPositionX < mPositionX) {mYVelocity+=0.001; mXVelocity +=0.000; mPositionY+=5; } // +Y
            if (mAngle > 80 && mAngle < 91 && mP2.mPositionY < mPositionY && mP2.mPositionX > mPositionX) {mYVelocity+=0.001; mXVelocity +=0.000; mPositionY+=5; } // +Y
            if (mAngle > 80 && mAngle < 91 && mP2.mPositionY < mPositionY && mP2.mPositionX == mPositionX) {mYVelocity+=0.001; mXVelocity +=0.000; mPositionY+=5; } // +Y
            if (mAngle > 80 && mAngle < 91 && mP2.mPositionY > mPositionY && mP2.mPositionX > mPositionX) {mYVelocity-=0.001; mXVelocity +=0.000; mPositionY-=5; } // -Y
            if (mAngle > 80 && mAngle < 91 && mP2.mPositionY > mPositionY && mP2.mPositionX < mPositionX) {mYVelocity-=0.001; mXVelocity +=0.000; mPositionY-=5; } // -Y
            if (mAngle > 80 && mAngle < 91 && mP2.mPositionY > mPositionY && mP2.mPositionX == mPositionX) {mYVelocity-=0.001; mXVelocity +=0.000; mPositionY-=5; } // -Y



            if (mYVelocity >= 0.008) {
                mYVelocity =mYVelocity-0.001f;
            }
            if (mXVelocity >= 0.008) {
                mXVelocity = mXVelocity-0.001f;
            }
        }

    }

    private boolean checkCollisionWithPlayer(Player player){

        double xMod = mRadius;
        double yMod = mRadius;

        mDistanceX = (((player.mPositionX + xMod)-mPositionX) * ((player.mPositionX+xMod)-mPositionX));
        mDistanceY = (((player.mPositionY + yMod)-mPositionY) * ((player.mPositionY+yMod)-mPositionY));

        mDistance = ((mDistanceX) + (mDistanceY));

        double minDistance = ((player.mRadius + mRadius)*(player.mRadius + mRadius));

        if (mDistance < minDistance)
        {
            Log.d("collision","paddle");
        return true;
        }
        return  false;
    }

    private void checkCollisionWithWall(){
        //top wall
        if (mYVelocity < 0.0 && mPositionY - mRadius <= 0){
                mYVelocity = -mYVelocity;
            Log.d("collision","top wall");
        }
        //bottom wall
        if (mYVelocity > 0.0 && mPositionY + mRadius >= mMaxY){
            mYVelocity = -mYVelocity;
            Log.d("collision","bottom wall");
        }
        //right wall
        if (mXVelocity > 0.0 && mPositionX + mRadius >= mMaxX){
            mXVelocity = -mXVelocity;
        }
        //left wall
        if (mXVelocity < 0.0 && mPositionX - mRadius <= 0){
            mXVelocity = -mXVelocity;
        }
    }


    @Override
    public void onUpdate(long elapsedMillis, GameEngine gameEngine) {
        // Get the info from the inputController
        updatePosition(elapsedMillis, gameEngine.mInputController);
        checkCollisionWithWall();
        //checkFiring(elapsedMillis, gameEngine);
    }

    public void onDraw(){
        mPuck.animate().translationX((int) mPositionX).translationY((int) mPositionY)
                .setDuration(1)
                .start();
    }




}
