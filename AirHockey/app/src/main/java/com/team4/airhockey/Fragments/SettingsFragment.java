package com.team4.airhockey.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.InputDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.team4.airhockey.HockeyActivity;
import com.team4.airhockey.HockeyBaseFragment;
import com.team4.airhockey.R;
import com.team4.airhockey.engine.MusicManager;


/**
 * Created by ryu24 on 2017-05-01.
 */

public class SettingsFragment extends HockeyBaseFragment  {
    public SettingsFragment(){}

    private static final String PREF_SHOULD_DISPLAY_GAMEPAD_HELP = "com.example.yass.display.gamepad.help.boolean";
    private SeekBar seekBarMusic;
    private SeekBar seekBarSound;
    int mMap;
    private static MusicManager mInstance;
    private MediaPlayer mMediaPlayer;
    private  View mView;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    public int musicOn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings_menu, container, false);
        return rootView;

    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        mView = view;

        sharedPref = ((Activity)mView.getContext()).getPreferences(Context.MODE_PRIVATE);
        editor = sharedPref.edit();

        seekBarMusic = (SeekBar)(view.findViewById(R.id.bar_volumemusic));
        seekBarSound = (SeekBar)(view.findViewById(R.id.bar_volumesound));
        mMediaPlayer = MediaPlayer.create(view.getContext().getApplicationContext(), R.raw.red_doors);
        musicOn = sharedPref.getInt("musicOn",0);

        seekBarMusic.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
        int progress = 0;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {

                progress = progressValue;
              Toast.makeText(getActivity(), "Changing Music Volume",Toast.LENGTH_SHORT).show();
                if (progress == 0 && mMediaPlayer!=null){
                    mMediaPlayer.setVolume(0.0f,0.0f);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekBarSound.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            int progress = 0;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                progress = progressValue;
                Toast.makeText(getActivity(),"Changing Sounds Volume",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        view.findViewById(R.id.switch_musiconoff).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(musicOn == 1) {
                    // turn off music
                    if (musicOn != 0){
                        MusicManager.getInstance().stop();
                    }
                    musicOn = 0;
                }
                else{
                    MusicManager.getInstance().play((getActivity()), R.raw.red_doors);
                    musicOn = 1;
                }


            }
        });
        view.findViewById(R.id.switch_soundsonoff).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(musicOn == 0) {
//                    // turn on sounds
//
//                    musicOn = 1;
//                }
//                else{
//
//                }

            }
        });
        view.findViewById(R.id.btn_goback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putInt("musicOn",musicOn);
                editor.commit();
                ((HockeyActivity)getActivity()).mainMenu();
            }
        });


        mMap = sharedPref.getInt("saved_spinner2",0);
        Map(view,mMap);

    }

    private void Map(final View view,int choice){
        switch (choice){
            case 0:
                Drawable bg = view.getContext().getResources().getDrawable(R.drawable.hockeytable0);
                view.findViewById(R.id.settingsbg).setBackground(bg);
                break;
            case 1:
                Drawable bg2 = view.getContext().getResources().getDrawable(R.drawable.neonhockeytable);
                view.findViewById(R.id.settingsbg).setBackground(bg2);
                break;
            case 2:
                Drawable bg3 = view.getContext().getResources().getDrawable(R.drawable.airhockeysoccer);
                view.findViewById(R.id.settingsbg).setBackground(bg3);
                break;

        }

    }


    @Override
    public void onResume() {
        super.onResume();
        if (isGameControllerConnected() && shouldDisplayGamepadHelp()) {
            displayGamepadHelp();
            // Do not show the dialog again
            PreferenceManager.getDefaultSharedPreferences(getActivity())
                    .edit()
                    .putBoolean(PREF_SHOULD_DISPLAY_GAMEPAD_HELP, false)
                    .commit();
        }
    }

    private void displayGamepadHelp() {
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.gamepad_help_title)
                .setMessage(R.string.gamepad_help_message)
                .create()
                .show();
    }

    private boolean shouldDisplayGamepadHelp() {
        return PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getBoolean(PREF_SHOULD_DISPLAY_GAMEPAD_HELP, true);
    }

    public boolean isGameControllerConnected() {
        int[] deviceIds = InputDevice.getDeviceIds();
        for (int deviceId : deviceIds) {
            InputDevice dev = InputDevice.getDevice(deviceId);
            int sources = dev.getSources();
            if (((sources & InputDevice.SOURCE_GAMEPAD) == InputDevice.SOURCE_GAMEPAD) ||
                    ((sources & InputDevice.SOURCE_JOYSTICK) == InputDevice.SOURCE_JOYSTICK)) {
                return true;
            }
        }
        return false;


    }
}
